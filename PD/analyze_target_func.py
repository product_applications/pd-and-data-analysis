'''
File Description:
Filename: analyze_target_func.py
Purpose: this file only defines methods. If target or lidar resolution or lidar model changes, please modify the important section accordingly
How to use:
1. define target size. if not specified, the default values are, height=1.5m, width = 1.8m
2. define lidar resolution for horizontal and vertical directions
3. define lidar model - 'falcon' or 'cheeta' or 'jaguar'
4. (only if needed) if falcon, select the channel used for POD. Ignore the second channel values in the final results.

Mo Yang
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import csv
from data_crop_func import crop_data

def analyze_target(keyname,bounding_box = [],n_truncated=0,plt_show=True,show_single_frame = True):

    '''You only need to modify the following values in most cases
    '''

    target_w = 1.8 #horizontal width in meter
    target_h = 1.5 #vertical_height in meter
    # beam_size = 1.047 #beam size in mrad
    beam_size = 1.2  # beam size in mrad

    vertical_resolution = 1.33  # in mrad
    horizontal_resolution = 1.19  # in mrad

    #for cheetah
    # vertical_resolution = 4.88  # in mrad
    # horizontal_resolution = 2.27  # in mrad

    current_model = 'falcon'
    current_falcon_channel = 1 #0 or 1 or 2 or 3
    # current_model = 'cheetah'
    # current_model = 'jaguar'
    '''ENG OF COMMON MODIFICATION SECTION'''





    ################Data Selection - Start###############################
    df = pd.read_hdf('./data.h5',key = keyname) # Both channels

    df = df[df['frame_idx']>=n_truncated] # truncated the first 30 frames to avoid any warm-up effects

    #['frame_idx', 'flags', 'intensity', 'timestamp', 'x', 'y', 'z']
    xyz = df.values[:,[4,5,6,0,1,2]]



    totalN_frame = int(xyz[-1,3]-n_truncated+1)
    last_frame = int(xyz[-1,3])

    interest_xyz = crop_data(xyz,bounding_box)
    if current_model == 'falcon':
        interest_xyz_channel1 = interest_xyz[np.where(interest_xyz[:,4]%4==current_falcon_channel)]
        interest_xyz_channel2 = interest_xyz[np.where(interest_xyz[:,4]%4==current_falcon_channel)]

        #if view the 4 channels as a single pointcloud, then just index everything. Use the following two lines
        # interest_xyz_channel1 = interest_xyz[:,:]
        # interest_xyz_channel2 = interest_xyz[:,:]


    if current_model == 'cheetah':
        interest_xyz_channel1 = interest_xyz[np.where(interest_xyz[:, 4] % 2 == 0)]
        interest_xyz_channel2 = interest_xyz[np.where(interest_xyz[:, 4] % 2 == 1)]



    frame100_channel1 = interest_xyz_channel1[np.where((interest_xyz_channel1[:,3]>100)&(interest_xyz_channel1[:,3]<130))]
    frame100_channel2 = interest_xyz_channel2[np.where((interest_xyz_channel2[:,3]>100)&(interest_xyz_channel2[:,3]<130))]

    avg_target_xyz = np.mean(interest_xyz, axis=0)
    avg_target_xyz_1 = np.mean(interest_xyz_channel1, axis=0)
    avg_target_xyz_2 = np.mean(interest_xyz_channel2, axis=0)

    # plt.figure(1)
    # plt.plot(range(len(interest_xyz_channel1[:,-3])), interest_xyz_channel1[:,-1])
    # plt.show()
    print('                    ')
    print('Center of the target - X, Y, Z','AVG Intensity')
    print('Overall Center', round(avg_target_xyz[0],4), round(avg_target_xyz[1],4), round(avg_target_xyz[2],4),'AVG Intensity', round(np.mean(interest_xyz[:,-1], axis=0),2))
    print('Chan1 Center  ', round(avg_target_xyz_1[0],4), round(avg_target_xyz_1[1],4), round(avg_target_xyz_1[2],4), 'AVG Intensity',round(np.mean(interest_xyz_channel1[:, -1], axis=0),2))
    print('Chan2 Center  ', round(avg_target_xyz_2[0],4), round(avg_target_xyz_2[1],4), round(avg_target_xyz_2[2],4), 'AVG Intensity',round(np.mean(interest_xyz_channel2[:, -1], axis=0),2))
    ################Data Selection - End###############################



    ################Empty Frame Analysis - Start###############################
    frame_list_overall = interest_xyz[:,3]
    frame_list_channel1 = interest_xyz_channel1[:,3]
    frame_list_channel2 = interest_xyz_channel2[:,3]
    frame_num, p_counts = np.unique(frame_list_overall,return_counts = True)
    frame_num1, p_counts1 = np.unique(frame_list_channel1,return_counts = True)
    frame_num2, p_counts2 = np.unique(frame_list_channel2,return_counts = True)

    frame_without_point = totalN_frame-len(frame_num)
    frame_without_point1 = totalN_frame-len(frame_num1)
    frame_without_point2 = totalN_frame-len(frame_num2)
    print('                    ')
    print('<<Empty Frame Rate>>')
    print('Overall',round(frame_without_point/totalN_frame,3),
          'Channel 1: ',round(frame_without_point1/totalN_frame,3),
          'Channel 2: ',round(frame_without_point2/totalN_frame,3))

    frame_num = frame_num.tolist()
    frame_num1 = frame_num1.tolist()
    frame_num2 = frame_num2.tolist()
    p_counts = p_counts.tolist()
    p_counts1 = p_counts1.tolist()
    p_counts2 = p_counts2.tolist()
    #
    new_frame_num = []
    newP_counts = []
    new_frame_num1 = []
    newP_counts1 = []
    new_frame_num2 = []
    newP_counts2 = []

    for i in range(n_truncated, (last_frame+1)):
        new_frame_num.append(i)
        new_frame_num1.append(i)
        new_frame_num2.append(i)

        if float(i) in frame_num:
            newP_counts.append(p_counts[0])
            p_counts.pop(0)
        else:
            newP_counts.append(0)

        if float(i) in frame_num1:
            newP_counts1.append(p_counts1[0])
            p_counts1.pop(0)
        else:
            newP_counts1.append(0)

        if float(i) in frame_num2:
            newP_counts2.append(p_counts2[0])
            p_counts2.pop(0)
        else:
            newP_counts2.append(0)

    print('                    ')
    print('<<Points per Frame Analysis>> - using shrinked box to exclude partial hits')
    print('AVG P/F       :',round(np.mean(newP_counts),3), '    STDEV P/F     :',round(np.std(newP_counts),3),'    RANGE P/F     :',round(np.ptp(newP_counts),3))
    print('AVG P/F  Chan1:',round(np.mean(newP_counts1),3),'    STDEV P/F1    :',round(np.std(newP_counts1),3),'    RANGE P/F1    :',round(np.ptp(newP_counts1),3))
    print('AVG P/F  Chan2:',round(np.mean(newP_counts2),3),'    STDEV P/F2    :',round(np.std(newP_counts2),3),'    RANGE P/F2    :',round(np.ptp(newP_counts2),3))

    plt.figure(1)
    plt.plot(new_frame_num,newP_counts)
    plt.title('Number of points per frame')
    plt.xlabel('frame #')
    plt.ylabel('Detected Points')
    plt.savefig(keyname+'_pointsVSframe.png')

    if plt_show:
        plt.show()
    plt.close()
    ################Empty Frame Analysis - End###############################



    ################Point Spread Visualization - Start###############################
    fig = plt.figure(2)

    if show_single_frame:
        ax2 = fig.add_subplot(111, projection='3d')
        ax2.scatter(frame100_channel1[:,1],frame100_channel1[:,2],frame100_channel1[:,0], c='r', marker='o',s = 7)
        ax2.scatter(frame100_channel2[:,1],frame100_channel2[:,2], frame100_channel2[:,0], c='y', marker='o',s = 7)
    else:
        ax2 = fig.add_subplot(111)
        ax2.scatter(interest_xyz_channel1[:,1],interest_xyz_channel1[:,0], c='r', marker='o',s = 7)
        ax2.scatter(interest_xyz_channel2[:,1], interest_xyz_channel2[:,0], c='y', marker='o',s = 7)


    ax2.set_xlabel('Y Axis (m)')
    ax2.set_ylabel('X Axis (m)')
    # ax2.set_zlabel('X Axis (m)')
    # plt.axis('equal')
    # ax2.set_facecolor('k')
    plt.savefig(keyname+'_AllFrames.png')
    if plt_show:
        plt.show()
    plt.close()
    print('END')
    ################Point Spread Visualization - End###############################



    ################Small Box Analysis for POD - Start###############################
    chan1_x = avg_target_xyz_1[0]
    chan1_y = avg_target_xyz_1[1]
    chan1_z = avg_target_xyz_1[2]
    chan2_x = avg_target_xyz_2[0]
    chan2_y = avg_target_xyz_2[1]
    chan2_z = avg_target_xyz_2[2]

    beam_size_offset = beam_size*chan1_z/1000
    w_offset = (target_w-beam_size_offset)/2
    h_offset = (target_h-beam_size_offset) / 2

    smaller_bounding_box_chan1 = [chan1_x-h_offset,chan1_x+h_offset,chan1_y-w_offset,chan1_y+w_offset,chan1_z-0.2,chan1_z+0.2]
    smaller_bounding_box_chan2 = [chan2_x - h_offset, chan2_x + h_offset, chan2_y - w_offset, chan2_y + w_offset,chan2_z - 0.2, chan2_z + 0.2]
    smaller_data1 = crop_data(interest_xyz_channel1, smaller_bounding_box_chan1)
    smaller_data2 = crop_data(interest_xyz_channel2, smaller_bounding_box_chan2)

    smaller_frame_list_channel1 = smaller_data1[:, 3]
    smaller_frame_list_channel2 = smaller_data2[:, 3]
    smaller_frame_num1, smaller_p_counts1 = np.unique(smaller_frame_list_channel1, return_counts=True)
    smaller_frame_num2, smaller_p_counts2 = np.unique(smaller_frame_list_channel2, return_counts=True)

    smaller_frame_num1 = smaller_frame_num1.tolist()
    smaller_frame_num2 = smaller_frame_num2.tolist()
    smaller_p_counts1 = smaller_p_counts1.tolist()
    smaller_p_counts2 = smaller_p_counts2.tolist()
    #

    NEWsmaller_frame_num1 = []
    NEWsmaller_p_counts1 = []
    NEWsmaller_frame_num2 = []
    NEWsmaller_p_counts2 = []

    for i in range(n_truncated, (last_frame + 1)):
        NEWsmaller_frame_num1.append(i)
        NEWsmaller_frame_num2.append(i)

        if float(i) in smaller_frame_num1:
            NEWsmaller_p_counts1.append(smaller_p_counts1[0])
            smaller_p_counts1.pop(0)
        else:
            NEWsmaller_p_counts1.append(0)

        if float(i) in smaller_frame_num2:
            NEWsmaller_p_counts2.append(smaller_p_counts2[0])
            smaller_p_counts2.pop(0)
        else:
            NEWsmaller_p_counts2.append(0)
    print('                    ')
    print('<<Probability of Detection>>')

    print('AVG P/F Chan1    :', round(np.mean(NEWsmaller_p_counts1),3), '    STDEV Chan1    :', round(np.std(NEWsmaller_p_counts1),3), '    RANGE Chan1    :', round(np.ptp(NEWsmaller_p_counts1),3))
    print('AVG P/F Chan2    :', round(np.mean(NEWsmaller_p_counts2),3), '    STDEV Chan2    :', round(np.std(NEWsmaller_p_counts2),3), '    RANGE Chan2    :', round(np.ptp(NEWsmaller_p_counts2),3))


    theoretical_num_p = ((target_h - beam_size_offset) / (vertical_resolution * chan1_z / 1000)) * (
            (target_w - beam_size_offset) / (horizontal_resolution * chan1_z / 1000))

    print('                    ')
    print('Theoretical detected number of points:    ',round(theoretical_num_p,2) )
    print('AVG PD-chan1:  ', round(np.mean(NEWsmaller_p_counts1)*100/theoretical_num_p,3),'%')
    print('AVG PD-chan2:  ', round(np.mean(NEWsmaller_p_counts2)*100/theoretical_num_p,3),'%')

    ################Small Box Analysis for POD - End###############################



    ################Analysis Result Output - Start###############################
    row = [keyname,np.mean(newP_counts),np.std(newP_counts),np.ptp(newP_counts),\
           np.mean(newP_counts1),np.std(newP_counts1),np.ptp(newP_counts1),\
            np.mean(newP_counts2),np.std(newP_counts2),np.ptp(newP_counts2), \
           frame_without_point / totalN_frame, \
           frame_without_point1 / totalN_frame, \
           frame_without_point2 / totalN_frame,\
        avg_target_xyz[0],avg_target_xyz[1],avg_target_xyz[2], \
           avg_target_xyz_1[0], avg_target_xyz_1[1], avg_target_xyz_1[2],\
           avg_target_xyz_2[0], avg_target_xyz_2[1], avg_target_xyz_2[2],\
        np.mean(interest_xyz[:,-1], axis=0),\
        np.median(interest_xyz[:,-1], axis=0),\
        np.ptp(interest_xyz[:,-1], axis=0),\
        np.std(interest_xyz[:,-1], axis=0), \
           np.mean(smaller_data1[:, -1], axis=0), \
           np.mean(smaller_data2[:, -1], axis=0), \
           np.mean(NEWsmaller_p_counts1), \
           np.std(NEWsmaller_p_counts1), \
           np.ptp(NEWsmaller_p_counts1),\
           np.mean(NEWsmaller_p_counts2), \
           np.std(NEWsmaller_p_counts2), \
           np.ptp(NEWsmaller_p_counts2), \
           np.mean(NEWsmaller_p_counts1) / theoretical_num_p, \
           np.mean(NEWsmaller_p_counts2) / theoretical_num_p


           ]

    with open('analysis.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(row)

    csvFile.close()
    ################Analysis Result Output - End###############################