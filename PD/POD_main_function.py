'''
File Description:
Filename: POD_main_function.py
Purpose: user to enter the target center and corresponding bounding box so it calls the rest of the POD code to finish analysis
How to use:
1. use Rviz to find the target center for analysis
2. put the center(X,Y,Z) into the target_center dictionary
3. define the corresponding bounding box for that specific target
4. (only if needed) enter the number of frames need to be truncated. THe default is always 30 frames

Mo Yang
'''

import csv
from load_data_func import load_data
from analyze_target_func import analyze_target

if __name__ == '__main__':

    #specify the target center coordinates - x,y,z from the raw pointcloud
    target_center = {
        'FB1_FIR37_200M': [-4.81,5.94,194.46],
    }


    # specify the large bounding box - x,y,z from the raw pointcloud
    #   [xlim,ylim,zlim]
    #   or
    #   [x_ll,x_ul,y_ll,y_ul,z_ul,z_ul]
    bounding_box_tolerance = {

        'FB1_FIR37_200M': [2, 2, 1],

    }

    #number of frames to drop before analyzing the data
    n_truncated = 30


    #write the data headers into the CSV
    row = ['Filename', 'Overall P/F AVG','Overall P/F STD','Overall P/F RANGE',
           'Chan1 P/F AVG','Chan1 P/F STD','Chan1 P/F RANGE',
           'Chan2 P/F AVG','Chan2 P/F STD','Chan2 P/F RANGE',
           'Empty%','Chan1-Empty%','Chan2-Empty%',
           'X','Y','Z','X1','Y1','Z1','X2','Y2','Z2',
           'Intensity AVG','Median','STD','RANGE',
           'Chan1_intensity','Chan2_intensity',
           'AVG P/F Chan1', 'STDEV Chan1', 'RANGE Chan1',
           'AVG P/F Chan2', 'STDEV Chan2', 'RANGE Chan2',
           'AVG PD-chan1',
           'AVG PD-chan2'
           ]
    with open('analysis.csv', 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(row)
    csvFile.close()


    #Analyze each targets in the target_center dictionary
    for keyname in target_center:
        print('Analysis Result for ' + keyname)
        if len(bounding_box_tolerance[keyname])>3:
            x_min = target_center[keyname][0] - bounding_box_tolerance[keyname][0]
            x_max = target_center[keyname][0] + bounding_box_tolerance[keyname][1]
            y_min = target_center[keyname][1] - bounding_box_tolerance[keyname][2]
            y_max = target_center[keyname][1] + bounding_box_tolerance[keyname][3]
            z_min = target_center[keyname][2] - bounding_box_tolerance[keyname][4]
            z_max = target_center[keyname][2] + bounding_box_tolerance[keyname][5]
        else:
            x_min = target_center[keyname][0] - bounding_box_tolerance[keyname][0]
            x_max = target_center[keyname][0] + bounding_box_tolerance[keyname][0]
            y_min = target_center[keyname][1] - bounding_box_tolerance[keyname][1]
            y_max = target_center[keyname][1] + bounding_box_tolerance[keyname][1]
            z_min = target_center[keyname][2] - bounding_box_tolerance[keyname][2]
            z_max = target_center[keyname][2] + bounding_box_tolerance[keyname][2]

        #Define bounding box for each target
        bounding_box = [x_min,x_max,y_min,y_max,z_min,z_max]

        #Start analysis
        # the inputs are 1)keyname 2)bounding_box 3)number of frames truncated in the beginning 4)if to show plot 5)if show partial plot or all the frames plot
        analyze_target(keyname,bounding_box,n_truncated,True,False)