import numpy as np

def crop_data(raw_data, bounding_box):

    x_LL = bounding_box[0]
    x_UL = bounding_box[1]
    y_LL = bounding_box[2]
    y_UL = bounding_box[3]
    z_LL = bounding_box[4]
    z_UL = bounding_box[5]

    result_data = raw_data[np.where((x_LL <= raw_data[:, 0]) & (raw_data[:, 0] <= x_UL) & \
                                (y_LL <= raw_data[:, 1]) & (raw_data[:, 1] <= y_UL) & \
                                (z_LL <= raw_data[:, 2]) & (raw_data[:, 2] <= z_UL))]
    return result_data

