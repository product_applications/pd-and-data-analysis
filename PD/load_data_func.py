'''
File Description:
Filename: load_data_func.py
Purpose: to load the data as data.h5 so we can process multiple csv files faster
How to use: provide the csv filename and place in the main script method, the method then calls the load_data function to complete the action

Mo Yang
'''

import pandas as pd



'''
load_data method - DO NOT MODIFY IF NEEDED!

This load_data method does the actual loading process
'''
def load_data(filename_list = [],data_path = []):
    if not filename_list:
        raise Exception('Please supply the data file')
    elif not data_path:
        raise Exception('Please supply the data path')
    else:
        for i in range(len(filename_list)):
            if i == 0:
                data_mode = 'w'
            else:
                data_mode = 'a'
            keyname = filename_list[i]
            full_keyname = keyname + '.csv'
            temp = pd.read_csv(data_path+ full_keyname)
            temp.to_hdf('data.h5', key=keyname,mode = data_mode)




'''
main script - Modify the data_list 

Put the corresponding CSV files in the list
'''
if __name__ == '__main__':
    data_loaded = False

    #Specify the CSV files here. .csv will automatically added. Only names are needed here.
    data_path = './'


    # The following is an example
    data_list = ['FB1_FIR37_150M','FB1_FIR37_200M']


    if not data_loaded:
        load_data(data_list, data_path)
